extends PathFollow3D

@onready var raycast = $Enemy/RayCast3D
var Speed = .05


func _process(delta:float) -> void:
	set_progress_ratio(get_progress_ratio() + Speed * delta)
	if raycast.is_colliding():
		print(raycast.get_collision_mask_value())
		print("collision detected")
	
