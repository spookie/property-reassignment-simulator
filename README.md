# Property Reassignment Simulator
Your task is to steal the paintings watched over by the flying saucer!

## Running
1. Download the Godot 4.0 (https://godotengine.org/)
2. Open project.godot file with it and run

## Features
* Terrain created via displacement of a grid with a heightmap taken from the Copernicus European programme (EU-DEM dataset, tile E20N20)
    + The location being: Mogadouro, Portugal
* Physically based materials used throughout, special attention given to the building itself
* The flying saucer glides along a predefined path, uses a spotlight for vision, which would (in theory) collide with the player and begin following it
* Grass shader with simulated wind effect. Which... doesn't work in stereoscopic 3D
* Hand tracking, limited to Oculus/Meta devices capabilities (no individual finger tracking)
* Pickable objects with proper physics and collision
    + Your hands snap to predefined locations, grabbable with both left, and right hands
    + You're able to throw them in the air, and they'll collide with your hands
* Proven to be working with any OpenXR capable device, given the minimum controller requirements
* Headset refresh rate identification and setup
* Running in both Windows, and GNU/Linux based machines

## Modelling, materials, and texture work
* Created with Blender
    + Terrain (UV mapping + texture painting + displacement modifier with heightmap)
    + Building (modelling + materials for both the floor and roof (UV mapping with proper orientation))
    + Saucer (modelling + materials)
    + Paintings (modelling)
    + Columns (modelling)
* Created with Godot
    + Building (remaining materials)
    + Paintings (texture work, and materials)
    + Columns (materials)
    + Grass shader with the GDShader language (closely related to GLSL)
    + Sky shader with the GDShader language (closely related to GLSL)

## Pictures
### Spawn location
![Spawn location](images/start.png)

### Exterior walls material detail (normal + ambient occlusion + roughness + specular mapping)
![Exterior walls](images/wall_exterior_material.png)

### Saucer
![Saucer drone](images/saucer.png)

### World map
![Map overview](images/world.png)

### Paintings with snap zones
![Paintings](images/paintings.png)

## Project structure
* addons/
    + godot-xr-tools/ (for OpenXR compatibility)
    + blender_helper/ (for automatic import of models)
    + godot-git-plugins/ (git integration)
* assets/
    + BlenderModels/ (raw blender files and textures)
    + bushes/ (the tree models surronding the building (taken from opengameart.org))
    + environment/ (camera environment properties)
    + grass/ (grass textures that were too be used in billboards)
    + images/ (first directory for textures)
    + materials/ (materials created in Godot)
    + museum/ (model and texture work)
    + shaders/ (sky and grass GDShader files)
* images/ (used for this README)
* scenes/ (game worlld structured into each of its components)
    + building
    + enemy
    + grass
    + knight (taken from opengameart.org as well)
    + main
    + map
    + player (contains the necessary XR configuration)
* scripts/ (game logic)
    + enemy (following player mechanism)
    + enemy_kernel (signal propagation kernel tied to the physics processing)
    + main (performs OpenXR setup, initializes the interface, checks if passthrough is enabled when headset is found)
    + navigation (updates player location for the enemy script every frame, info is then used on the enemy_kernel script every 5 frames to signal any enemy on the "enemies" group)


## Essential RTFM
https://docs.godotengine.org/en/stable/tutorials/xr/index.html
