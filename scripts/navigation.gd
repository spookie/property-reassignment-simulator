extends "res://scripts/main.gd"

@onready var agent: RigidBody3D = $Museum/MuseumModel/NavigationRegion3D/Agent
@onready var target: Node = $Player/Player

func _physics_process(delta):
	if agent:
		agent.set_target_position(target.global_transform.origin)		

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
