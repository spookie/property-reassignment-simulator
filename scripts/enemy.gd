extends RigidBody3D

@onready var nav_agent = $NavigationAgent3D

var SPEED = 1.0
var movement_delta : float

func _physics_process(delta):
	movement_delta = SPEED * delta
	var current_location = global_transform.origin
	var next_location = nav_agent.get_next_path_position()
	var new_velocity = (next_location - current_location).normalized() * SPEED
	nav_agent.set_velocity(new_velocity)
	global_transform.origin = global_transform.origin.move_toward(global_transform.origin + new_velocity, movement_delta)
	

func update_target_position(target_position) -> void:
	nav_agent.set_target_position(target_position)
