extends "res://scripts/main.gd"

@onready var player = $XROrigin3D/PlayerBody
@onready var raycast = $Path3D/PathFollow3D/Enemy/RayCast3D

func _physics_process(delta):
	get_tree().create_timer(5.0)
	get_tree().call_group("enemies", "update_target_position", player.global_transform.origin)
